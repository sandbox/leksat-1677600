
== Overview ==

The module gathers statistics about outgoing emails and provides several
reports. This includes:

- how many email (by which module, when) was sent,
- how many times an email was opened (by which user, when),
- what links was clicked (how many times, when),
- charts for reports.

== Install ==

Install module as usual (http://drupal.org/node/895232). Configure it settings
at Administration » Configuration » System » Mail Statistics.

== Usage ==

The module starts collect statistics about outgoing emails right after enabling.
By default it process only links contained in "href" attributes, but you can
change it in the module's settings. Information about email opens collects only
if your system is configured to send email in HTML format.

After some information is collected, you can view reports at Administration »
Reports » Mail Statistics.
