<?php

/**
 * @file
 * Misc functions for Mail Statistics module.
 */

/**
 * Registers click.
 *
 * @param string $link_id
 *   The {mail_statistics_link}.id of a link.
 * @param int $timestamp
 *   Timestamp to register.
 * @param int $mail_id
 *   The {mail_statistics_mail}.id of an email.
 * @param int $send_id
 *   The {mail_statistics_send}.id of an email.
 * @param string $send_time
 *   The {mail_statistics_mail}.send_time of an email.
 */
function _mail_statistics_register_click($link_id, $timestamp, $mail_id, $send_id, $send_time) {
  $fields = array(
    'link_id' => $link_id,
  );
  $click_id = db_insert('mail_statistics_click')->fields($fields)->execute();
  _mail_statistics_register_action('click', $click_id, $timestamp, $mail_id, $send_id, $send_time);

  // If mail open wasn't detected by hidden image, register it now.
  if (!_mail_statistics_is_open_registered($send_id)) {
    _mail_statistics_register_open($timestamp, $mail_id, $send_id, $send_time);
  }
}

/**
 * Checks whether an open was registered for the given send.
 *
 * @param int $send_id
 *   The {mail_statistics_send}.id of an email.
 *
 * @return bool
 */
function _mail_statistics_is_open_registered($send_id) {
  return (bool) db_select('mail_statistics_open', 'o')
    ->fields('o', array('id'))
    ->condition('o.send_id', $send_id)
    ->execute()
    ->fetchField();
}

/**
 * Registers mail open.
 *
 * @param int $timestamp
 *   The timestamp to register.
 * @param int $mail_id
 *   The {mail_statistics_mail}.id of an email.
 * @param int $send_id
 *   The {mail_statistics_send}.id of an email.
 * @param string $send_time
 *   The {mail_statistics_mail}.send_time of an email.
 */
function _mail_statistics_register_open($timestamp, $mail_id, $send_id, $send_time) {
  $fields = array(
    'send_id' => $send_id,
  );
  $open_id = db_insert('mail_statistics_open')->fields($fields)->execute();
  _mail_statistics_register_action('open', $open_id, $timestamp, $mail_id, $send_id, $send_time);
}

/**
 * Registers an action.
 *
 * @param string $action
 *   The {mail_statistics_action}.action value ("send", "open", or "click").
 * @param int $action_id
 *   The ID of registered action.
 * @param int $timestamp
 *   The timestamp to register.
 * @param int $mail_id
 *   The {mail_statistics_mail}.id of an email.
 * @param int $send_id
 *   The {mail_statistics_send}.id of an email.
 * @param string $send_time
 *   The {mail_statistics_mail}.send_time of an email.
 */
function _mail_statistics_register_action($action, $action_id, $timestamp, $mail_id, $send_id, $send_time) {
  $fields = array(
    'action' => $action,
    'action_id' => $action_id,
    'time' => _mail_statistics_format_time_string($timestamp),
    'mail_id' => $mail_id,
    'send_id' => $send_id,
    'send_time' => $send_time,
  );
  db_insert('mail_statistics_action')->fields($fields)->execute();
}

/**
 * Converts timestamp to "YmdHis" time string.
 *
 * @param int $timestamp
 *   A unix timestamp.
 *
 * @return string
 *   Formatted time string.
 */
function _mail_statistics_format_time_string($timestamp) {
  static $cache = array();
  if (empty($cache[$timestamp])) {
    $cache[$timestamp] = format_date($timestamp, 'custom', 'YmdHis');
  }
  return $cache[$timestamp];
}

/**
 * Generates an universally unique identifier.
 */
function _mail_statistics_generate_uuid() {
  if (module_exists('uuid')) {
    return uuid_generate();
  }
  // Generates a UUID v4 using PHP code.
  // From http://php.net/uniqid#65879
  return sprintf('%04x%04x-%04x-%03x4-%04x-%04x%04x%04x',
    mt_rand(0, 65535), mt_rand(0, 65535),
    mt_rand(0, 65535),
    mt_rand(0, 4095),
    bindec(substr_replace(sprintf('%016b', mt_rand(0, 65535)), '01', 6, 2)),
    mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535)
  );
}

/**
 * Returns module human name.
 *
 * @param string $module
 *   Machine name of a module.
 *
 * @return string
 *   Module human name.
 */
function _mail_statistics_get_module_name($module) {
  $modules_info = system_get_info('module');
  return isset($modules_info[$module]['name'])
    ? $modules_info[$module]['name'] : $module;
}

/**
 * Returns report setting.
 *
 * @param string $setting
 *   Setting name.
 *
 * @return mixed
 *   Stored setting value or default value.
 */
function _mail_statistics_get_setting($setting) {
  $defaults = array(
    'group_by' => 'time',
    'time_grouping' => 8,
    'time_grouping_detailed' => 8,
    'rows_per_page' => 10,
    'rows_per_page_detailed' => 10,
  );
  return isset($_SESSION['mail_statistics_' . $setting])
    ? $_SESSION['mail_statistics_' . $setting]
    : (isset($defaults[$setting]) ? $defaults[$setting] : NULL);
}

/**
 * Saves report setting value into the $_SESSION array.
 *
 * @param string $setting
 *   Setting name.
 * @param mixed $value
 *   Setting value.
 */
function _mail_statistics_set_setting($setting, $value) {
  $_SESSION['mail_statistics_' . $setting] = $value;
}

/**
 * Returns cut time expression.
 *
 * @param string $field
 *   A field name to use.
 * @param bool $main_report
 *   Indicates which variable use: main or detailed report.
 *
 * @return string
 *   SQL expression compatible with mysql and postgresql.
 */
function _mail_statistics_get_time_expression($field, $main_report = TRUE) {
  $setting = $main_report ? 'time_grouping' : 'time_grouping_detailed';
  $time_grouping = _mail_statistics_get_setting($setting);
  return 'SUBSTRING(' . $field . ' FROM 1 FOR ' . $time_grouping . ')';
}

/**
 * Formats "human" date/time string from cut time string.
 *
 * @param string $time
 *   Time string in "YmdHis" format. May be cut.
 *
 * @return string
 *   Human readable date/time string.
 */
function _mail_statistics_format_time($time) {
  static $cache = array();
  if (empty($cache[$time])) {
    $d = ltrim(drupal_substr($time, 6, 2) . '.' . drupal_substr($time, 4, 2) . '.' . drupal_substr($time, 0, 4), '.');
    $t = rtrim(drupal_substr($time, 8, 2) . ':' . drupal_substr($time, 10, 2) . ':' . drupal_substr($time, 12, 2), ':');
    if (drupal_strlen($t) == 2) {
      $t = t('!hourh', array('!hour' => $t));
    }
    $cache[$time] = rtrim($d . ' ' . $t);
  }
  return $cache[$time];
}

/**
 * Adds chart for rendering.
 *
 * @param array $data
 *   An associative array with chart data/properties. Keys:
 *   - selector: jQuery selector of element which should contain chart.
 *   - type: chart class name (google.visualization callback).
 *   - data (optional): two dimension array with chart data. First row is a
 *     header.
 *   - options (optional): an array of options to pass to chart callback.
 */
function _mail_statistics_add_chart(array $data) {
  static $number = 0;
  $number++;
  $data += array(
    'data' => array(),
    'options' => array(),
  );

  // Add js files.
  drupal_add_js('https://www.google.com/jsapi', array('type' => 'external'));
  drupal_add_js(drupal_get_path('module', 'mail_statistics') . '/js/charts.js');

  // Chart data should be provided as numeric array.
  $data['data'] = array_map('array_values', array_values($data['data']));

  // Cast numeric values provided as strings because Google library is sensitive
  // to data types.
  foreach ($data['data'] as &$row) {
    foreach ($row as &$value) {
      if (is_string($value) && is_numeric($value)) {
        $value = (float) $value;
      }
    }
  }

  // Add chart data to JS settings.
  drupal_add_js(array(
    'mailStatistics' => array(
      'charts' => array(
        'chart-' . $number => $data,
      ),
    ),
  ), 'setting');
}

/**
 * Returns an array of time grouping options.
 */
function _mail_statistics_get_time_grouping_options() {
  return array(
    4 => t('years'),
    6 => t('months'),
    8 => t('days'),
    10 => t('hours'),
    12 => t('minutes'),
    14 => t('seconds'),
  );
}
