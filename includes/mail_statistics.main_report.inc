<?php

/**
 * @file
 * Main report pages.
 */

/**
 * Callback for the main report page.
 *
 * If the $module and the $key are not given, overview report for all modules
 * will be rendered.
 *
 * @param string|null $module
 *   A machine name of a module.
 * @param string|null $key
 *   A mail key.
 *
 * @return array
 *   Report page's render array.
 */
function mail_statistics_report_page($module = NULL, $key = NULL) {
  _mail_statistics_include();
  drupal_add_css(drupal_get_path('module', 'mail_statistics') . '/css/common.css');
  $group_by_time = (_mail_statistics_get_setting('group_by') == 'time');
  $build = array();
  $build['form'] = drupal_get_form('mail_statistics_report_form');

  // This indicates do we need full report or filtered by module:key.
  $is_full_report = is_null($module) && is_null($key);

  if (!$is_full_report) {
    // Drupal displays only "Home", but we want full breadcrumbs.
    drupal_set_breadcrumb(array(
      l(t('Home'), '<front>'),
      l(t('Administration'), 'admin'),
      l(t('Reports'), 'admin/reports'),
      l(t('Mail Statistics'), 'admin/reports/mail_statistics'),
    ));
  }

  $header = array(
    'grouping_field' => array(
      'data' => $group_by_time ? t('Date/Time') : t('Campaign'),
      'field' => $group_by_time ? 'grouping_field' : 'campaign_name',
      'sort' => 'desc',
    ),
    'mails_count' => array(
      'data' => t('Mails sent'),
      'field' => 'mails_count',
      'sort' => 'desc',
    ),
    'opens_count' => array(
      'data' => t('Mail opens'),
      'field' => 'opens_count',
      'sort' => 'desc',
    ),
    'clicks_count' => array(
      'data' => t('Link clicks'),
      'field' => 'clicks_count',
      'sort' => 'desc',
    ),
  );
  if ($is_full_report) {
    $header['module:key'] = array(
      'data' => t('Module:key'),
    );
  }

  // For the full report there should be an additional column with list of
  // module:key of mails sent that time.
  if ($is_full_report) {
    $modules_activity = _mail_statistics_get_modules_activity();
  }

  // Build the query.
  $query = db_select('mail_statistics_action', 'a');
  if ($group_by_time) {
    $query->addExpression(_mail_statistics_get_time_expression('a.send_time'), 'grouping_field');
  }
  else {
    $query->addField('m', 'campaign_id', 'grouping_field');
    $query->addField('m', 'campaign_name');
    $query->addField('m', 'module');
    $query->addField('m', 'mail_key');
    $query->condition('m.campaign_id', '', '<>');
    $query->groupBy('m.module');
  }
  $query->groupBy('grouping_field');
  $query->addExpression("SUM(a.action = 'send')", 'mails_count');
  $query->addExpression("SUM(a.action = 'open')", 'opens_count');
  $query->addExpression("SUM(a.action = 'click')", 'clicks_count');
  $condition = 'm.id = a.mail_id';
  $arguments = array();
  if (!$is_full_report) {
    $condition .= ' AND m.module = :module AND m.mail_key = :key';
    $arguments[':module'] = $module;
    $arguments[':key'] = $key;
  }
  $query->innerJoin('mail_statistics_mail', 'm', $condition, $arguments);
  $query = $query
    ->extend('TableSort')
    ->orderByHeader($header);
  $query = $query
    ->extend('PagerDefault')
    ->limit(_mail_statistics_get_setting('rows_per_page'));

  // Prepare table rows.
  $table_rows = array();
  $chart_data = array();
  foreach ($query->execute() as $row) {
    $grouping_field_formatted = $group_by_time
      ? _mail_statistics_format_time($row->grouping_field)
      : check_plain($row->campaign_name);
    if (!$is_full_report) {
      $path = 'admin/reports/mail_statistics/' . $module . '/' . $key
        . '/' . $row->grouping_field;
      $grouping_field_formatted = l($grouping_field_formatted, $path);
    }
    $table_row = array(
      'grouping_field' => $grouping_field_formatted,
      'mails_count' => check_plain($row->mails_count),
      'opens_count' => check_plain($row->opens_count),
      'clicks_count' => check_plain($row->clicks_count),
    );
    if ($is_full_report) {
      if ($group_by_time) {
        $module_list = '';
        foreach ($modules_activity[$row->grouping_field] as $module_info) {
          $title = $module_info['module_name'] . ':' . $module_info['key'];
          $path = 'admin/reports/mail_statistics/' . $module_info['module'] . '/'
            . $module_info['key'] . '/' . $row->grouping_field;
          $module_list .= '<div class="module-key">' . l($title, $path) . '</div>';
        }
        $table_row['module:key'] = $module_list;
      }
      else {
        $title = _mail_statistics_get_module_name($row->module) . ':' . $row->mail_key;
        $path = 'admin/reports/mail_statistics/' . $row->module . '/'
          . $row->mail_key . '/' . $row->grouping_field;
        $table_row['module:key'] = l($title, $path);
      }
    }
    $table_rows[] = $table_row;
    $chart_data[$row->grouping_field] = array(
      'grouping_field' => strip_tags($grouping_field_formatted),
      'mails_count' => $row->mails_count,
      'opens_count' => $row->opens_count,
      'clicks_count' => $row->clicks_count,
    );
  }

  if (variable_get('mail_statistics_display_charts', TRUE) && count($chart_data) > 1) {
    // For the chart use date sorting always.
    ksort($chart_data);
    array_unshift($chart_data, array(
      $group_by_time ? t('Date/Time') : t('Campaign'),
      t('Mails sent'),
      t('Mail opens'),
      t('Link clicks'),
    ));
    $build['chart'] = array(
      '#type' => 'container',
      '#attributes' => array('id' => 'mail-statistics-chart'),
    );
    _mail_statistics_add_chart(array(
      'selector' => '#' . $build['chart']['#attributes']['id'],
      'type' => 'LineChart',
      'data' => $chart_data,
    ));
  }

  $build['table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $table_rows,
    '#empty' => t('No results found.'),
  );
  $build['pager'] = array(
    '#theme' => 'pager',
  );

  return $build;
}

/**
 * Report's form callback.
 */
function mail_statistics_report_form($form, &$form_state) {
  _mail_statistics_include();
  $form['#attributes']['class'][] = 'mail-statistics-report-form';

  $query = db_select('mail_statistics_mail', 'm');
  $query->distinct();
  $query->addField('m', 'module');
  $query->addField('m', 'mail_key');
  $query->orderBy('module');
  $query->orderBy('mail_key');
  $options = array('' => t('All mails'));
  foreach ($query->execute() as $row) {
    $options[$row->module . '/' . $row->mail_key]
      = _mail_statistics_get_module_name($row->module) . ':' . $row->mail_key;
  }
  $form['module_key'] = array(
    '#type' => 'select',
    '#title' => t('View report for'),
    '#options' => $options,
  );
  if (arg(3) && arg(4)) {
    $value = arg(3) . '/' . arg(4);
    if (isset($options[$value])) {
      $form['module_key']['#default_value'] = $value;
    }
  }

  $form['group_by'] = array(
    '#type' => 'radios',
    '#title' => t('Group by'),
    '#options' => array(
      'time' => t('time'),
      'campaign' => t('campaigns'),
    ),
    '#default_value' => _mail_statistics_get_setting('group_by'),
  );

  $form['time_grouping'] = array(
    '#type' => 'select',
    '#title' => t('Group time by'),
    '#default_value' => _mail_statistics_get_setting('time_grouping'),
    '#options' => _mail_statistics_get_time_grouping_options(),
    '#states' => array(
      'visible' => array(
        ':input[name="group_by"]' => array('value' => 'time'),
      ),
    ),
  );

  $form['rows_per_page'] = array(
    '#type' => 'select',
    '#title' => t('Rows per page'),
    '#default_value' => _mail_statistics_get_setting('rows_per_page'),
    '#options' => array(10 => 10, 20 => 20, 50 => 50, 100 => 100),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
  );

  return $form;
}

/**
 * Submit callback for module:key selection form.
 *
 * Stores report's display setting values in the session, then redirects user
 * to the selected report page.
 */
function mail_statistics_report_form_submit($form, $form_state) {
  _mail_statistics_set_setting('group_by', $form_state['values']['group_by']);
  _mail_statistics_set_setting('time_grouping', $form_state['values']['time_grouping']);
  _mail_statistics_set_setting('rows_per_page', $form_state['values']['rows_per_page']);
  $path = 'admin/reports/mail_statistics/' . $form_state['values']['module_key'];
  drupal_goto($path);
}

/**
 * Returns modules activity grouped by send time.
 *
 * @return array
 *   The time string is the key. Each nested array contains the following keys:
 *   "module", "module_name" (human name), and "key".
 */
function _mail_statistics_get_modules_activity() {
  $query = db_select('mail_statistics_mail', 'm')
    ->fields('m', array('module', 'mail_key'));
  $query->addExpression(_mail_statistics_get_time_expression('m.send_time'), 'send_time_cut');
  $result = $query
    ->groupBy('send_time_cut')
    ->groupBy('m.module')
    ->groupBy('m.mail_key')
    ->orderBy('m.module')
    ->orderBy('m.mail_key')
    ->execute();
  $activity = array();
  foreach ($result as $row) {
    $activity[$row->send_time_cut][] = array(
      'module' => $row->module,
      'module_name' => _mail_statistics_get_module_name($row->module),
      'key' => $row->mail_key,
    );
  }
  return $activity;
}
